﻿using EditFormWithAjax.Models;
using Microsoft.AspNetCore.Mvc;

namespace EditFormWithAjax.Controllers
{
    public class UserController : Controller
    {
        private readonly ApplicationDbContext _context;
        public UserController(ApplicationDbContext context)
        {
            _context = context;
        }
        
        public IActionResult Index()
        {
            IEnumerable<User> userslist = _context.Users.ToList();
            return View(userslist);
        }
        //GET
        public IActionResult AddUser()
        {
            return View();
        }
        [HttpPost]
        public IActionResult AddUser(/*[FromBody]*/User user)
        {
            if (ModelState.IsValid)
            {
                _context.Users.Add(user);
                _context.SaveChanges();

                return Json(new { Message = "User added successfully" });

            }

            return Json(new { Error = "Invalid input data" });
        }

        //GET
        public IActionResult EditUser(int id)
        {
            var user = _context.Users.Find(id);
            return View(user);

        }

        [HttpPost]
        public IActionResult EditUser(User user)
        {
            if (ModelState.IsValid)
            {
                _context.Users.Update(user);
                _context.SaveChanges();

                return Json(new { Message = "User updated successfully" });
            }

            return Json(new { Error = "Invalid input data" });
        }
        //public IActionResult EditUser(User updatedUser)
        //{
        //    if (ModelState.IsValid)
        //    {
        //        var existingUser = _context.Users.FirstOrDefault(u => u.Id == updatedUser.Id);

        //        if (existingUser != null)
        //        {
        //            // Update user details
        //            existingUser.FirstName = updatedUser.FirstName;
        //            existingUser.LastName = updatedUser.LastName;
        //            existingUser.Age = updatedUser.Age;
        //            existingUser.Salary = updatedUser.Salary;

        //            _context.SaveChanges();

        //            return Json(new { Message = "User updated successfully", user = existingUser });
        //        }

        //        return Json(new { Error = "User not found" });
        //    }

        //    return Json(new { Error = "Invalid input data" });
        //}

        [HttpPost]
        public JsonResult DeleteUser(int id)
        {
            var user = _context.Users.Find(id);
            if (user != null)
            {
                _context.Users.Remove(user);
                _context.SaveChanges();
                return Json(new { Message = "User deleted successfully" });
            }

            return Json(new { Error = "User not found" });
        }

    }
}
